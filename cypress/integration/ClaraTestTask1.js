import "@testing-library/cypress/add-commands"
/// <reference types="Cypress" />

const { each } = require("bluebird")

describe('Test ToDoListPage', function () {

    it('LaunchToDoListPage_Testcase', function () {

        //Launch the URL
        cy.visit(Cypress.config().baseUrl)
        //Assertions for Page Title, Add button, Enter Text and items in List
        cy.get('h1').should('contain.text', 'Simple ToDo List')
        cy.get('.btn').should('contain.text', 'Add')
        cy.get('.form-control').should('be.visible')
        cy.get('.label').should('contain.text', '0')
    })

    it('AddToDoList_SingleAddition_TestCase', function () {

        //Adding a Single Task in ToDo List
        cy.visit(Cypress.config().baseUrl)
        cy.get('.form-control').type('Eggs')
        cy.get('.btn').click()
        cy.get('.label').should('contain.text', '1')

        //Assertion for the added element
        cy.get('.checkbox > .ng-binding').should('be.visible')
        //Delete the added Task
        cy.get('.ng-binding > input').click()
        //Assertion to confirm if the Task is deleted
        cy.get('.label').should('contain.text', '0')
    })

    it('AddToDoList_Multi[leAddition_TestCase', function () {

        //Adding a Single Task in ToDo List
        cy.visit(Cypress.config().baseUrl)
        //Adding a 2 or more Task in ToDo List
        cy.get('.form-control').type('Get Laundry')
        cy.get('.btn').click()
        cy.get('.form-control').type('Get Shopping')
        cy.get('.btn').click()
        cy.get('.form-control').type('Visit Paris')
        cy.get('.btn').click()
        cy.get('.label').should('contain.text', '3')
        //Assertion for the added element
        cy.get('.checkbox > .ng-binding').should('be.visible')
        //Delete the added Task
        cy.get(':nth-child(1) > .ng-binding > input').click()
        cy.get(':nth-child(1) > .ng-binding > input').click()
        cy.get(':nth-child(1) > .ng-binding > input').click()

        //Assertion to confirm if the Task is deleted
        cy.get('.label').should('contain.text', '0')
    })
})